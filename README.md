# sys-info-flask

[![pipeline status](https://gitlab.com/sjugge/docker/sys-info-flask/badges/master/pipeline.svg)](https://gitlab.com/sjugge/docker/sys-info-flask/commits/master)

Rudimentary [Flask](https://flask.palletsprojects.com/) app which outputs system information. Intended for testing Docker setups.

## Registry

See https://gitlab.com/sjugge/docker/sys-info-flask/container_registry

## Run

```bash
docker run --rm -t \
  -v /etc/hostname:/opt/app/host/hostname \
  -v /etc/os-release:/opt/app/host/os-release \
  -v `pwd`/traceroute-hosts:/opt/app/host/traceroute-hosts \
  -p 5000:5000 \
  registry.gitlab.com/sjugge/docker/sys-info-flask:master
```

### Mounts

Mount data from the host to the container for additional details and insights.

| Source | Target | Comment |
| --- | --- | --- |
| `/etc/hostname` | `/opt/app/host/hostname` | Allows the host hostname to be shown in the report. |
| `/etc/os-release` | `/opt/app/host/os-release` | Allows the host release details to be shown in the report. |
| `/path/to/traceroute-hosts` | `/opt/app/host/traceroute-hosts` | Domain names to perform a `traceroute` on, one per line. |
